// File:        project5.cpp
// Description: driver file for Project 5
// Author:      Robert Dyer (rdyer@bgsu.edu)
// Course:      CS3350, Fall 2017
#include <iostream>
#include <algorithm>
#include "BinarySearchTree.cpp"

int failures = 0;

void testResult(int result, int expected) {
    if (result != expected) {
        std::cout << "test FAILED: expected(" << expected << ") but result(" << result << ")" << std::endl;
        failures++;
    }
}

void testTree(int *arr, int n) {
	BinarySearchTree<int> tree;
    for (int i = 0; i < n; i++)
        tree.add(arr[i]);

    std::sort(arr, arr + n);

	BinarySearchTreeIterator<int> iter = tree.begin();
    for (int i = 0; i < n; i++) {
        if (iter == tree.end()) {
            std::cout << "test FAILED: iterator reached end prematurely" << std::endl;
            failures++;
            break;
        }
        testResult(*iter, arr[i]);
        ++iter;
    }
}

int main(int argc, char **argv) {
    int arr1[1] = { 5 };
    testTree(arr1, 1);

    int arr6[6] = { 8, 2, 1, 5, 3, 7 };
    testTree(arr6, 6);

    int arr50[50] = { 32, 49, 24, 26, 14, 36, 41, 21, 43, 37, 33, 29, 13, 39, 47, 9, 15, 30, 10, 17, 28, 22, 25, 19, 44, 48, 1, 27, 16, 3, 45, 34, 18, 40, 7, 35, 46, 50, 20, 23, 12, 2, 6, 42, 38, 31, 5, 8, 4, 11 };
    testTree(arr50, 50);

    if (failures == 0)
        std::cout << "ALL TESTS PASSED" << std::endl;
    else
        std::cout << failures << " TESTS FAILED" << std::endl;

	return 0;
}
