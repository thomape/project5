all:
	g++ -g -std=c++11 project5.cpp NotFoundException.cpp PrecondViolatedExcep.cpp

test: all
	./a.out

clean:
	rm -f a.out *.o
