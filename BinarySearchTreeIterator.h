// File:        BinarySearchTreeIterator.h
// Description: header file shell for a binary search tree iterator
// Author:      Shannon Davis (svdavis@bgsu.edu)
// Author:      Thomas Errington (thomape@bgsu.edu)
// Course:      CS3350, Fall 2017
#ifndef _BINARY_SEARCH_TREE_ITERATOR
#define _BINARY_SEARCH_TREE_ITERATOR

#include <iterator>
#include "LinkedQueue.cpp"


template <class ItemType>
class BinarySearchTreeIterator : public std::iterator<std::input_iterator_tag, int> {
 private:
	 LinkedQueue<ItemType> check;

 public:
    BinarySearchTreeIterator(const BinarySearchTree<ItemType>* someTree, BinaryNode<ItemType>* itemPtr);

    const ItemType operator*();
    BinarySearchTreeIterator<ItemType> operator ++();
    bool operator == (const BinarySearchTreeIterator<ItemType>& rightHandSide) const;
    bool operator != (const BinarySearchTreeIterator<ItemType>& rightHandSide) const;
}; // end BinarySearchTreeIterator

#endif
