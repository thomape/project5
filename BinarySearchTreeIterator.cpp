// File:        BinarySearchTreeIterator.h
// Description: implementation file shell for a binary search tree iterator
// Author:      Shannon Davis (svdavis@bgsu.edu)
// Author:      Thomas Errington (thomape@bgsu.edu)
// Course:      CS3350, Fall 2017
#include "BinarySearchTreeIterator.h"

template <class ItemType>
BinarySearchTreeIterator<ItemType>::BinarySearchTreeIterator(const BinarySearchTree<ItemType>* someTree, BinaryNode<ItemType>* itemPtr)
{

		while (itemPtr != someTree->end())
		{
			check.enqueue(*itemPtr);
			++itemPtr;
		}





} // end constructor

template <class ItemType>
const ItemType BinarySearchTreeIterator<ItemType>::operator*()
{
	return *this;
} // end operator *

template <class ItemType>
BinarySearchTreeIterator<ItemType> BinarySearchTreeIterator<ItemType>::operator++()
{
	//ItemType highVal;
	//ItemType next;

	//for (int j = 1; j <= check.getLength(); j++)
	//{
	//	if (check.getEntry(j) > highVal)
	//		highVal = check.getEntry(j);
	//}


	//for (int i = 1; i <= check.getLength(); i++)
	//{
	//	if (check.getEntry(i) > *this)
	//		if (check.getEntry(i) < highVal)
	//		{
	//			next = check.getEntry(i);
	//		}
	//}

	if (check.peekFront() == *this)
		return *this;
	// need to keep going left until leaf

	

	
	

} // end operator ++

template <class ItemType>
bool BinarySearchTreeIterator<ItemType>::operator==(const BinarySearchTreeIterator<ItemType>& rightHandSide) const
{
	if (check.peekFront() == *rightHandSide)
		return true;
	else
		return false;
} // end operator ==

template <class ItemType>
bool BinarySearchTreeIterator<ItemType>::operator!=(const BinarySearchTreeIterator<ItemType>& rightHandSide) const
{
	if (check.peekfront() != *rightHandSide)
		return true;
	else
		return false;
} // end operator !=